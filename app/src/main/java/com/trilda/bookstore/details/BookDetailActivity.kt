package com.trilda.bookstore.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import com.trilda.bookstore.Book
import com.trilda.bookstore.R
import com.trilda.bookstore.databinding.ActivityBookDetailBinding
import timber.log.Timber


class BookDetailActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_BOOK_ID = "bookId"
    }

    lateinit var binding : ActivityBookDetailBinding
    lateinit var viewModel : BookDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBookDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bookId = intent.getIntExtra(EXTRA_BOOK_ID, 1)
        Timber.d("Book id =$bookId")

        val factory = BookDetailViewModelFactory(bookId)
        viewModel = ViewModelProvider(this, factory).get(BookDetailViewModel::class.java)
        viewModel.book.observe(this, Observer {book -> updateBook(book!!)})
    }

    private fun updateBook(book: Book) {
        with (binding) {
            Picasso.get()
                .load(book.pictureUrl)
                .placeholder(R.drawable.ic_placeholder_image)
                .into(detailBookCover)

            detailBookTitle.text = book.title
            detailBookAuthor.text = book.author
            detailBookSummary.text = book.summary
        }
    }
}
