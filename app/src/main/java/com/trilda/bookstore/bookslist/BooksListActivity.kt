package com.trilda.bookstore.bookslist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.lifecycle.Observer
import com.trilda.bookstore.Book
import com.trilda.bookstore.databinding.ActivityBooksListBinding
import com.trilda.bookstore.details.BookDetailActivity
import timber.log.Timber
import java.util.*

class BooksListActivity : AppCompatActivity(), BooksListAdapter.BooksListAdapterListener {

    private val viewModel: BooksListViewModel by viewModels()
    private lateinit var booksAdapter : BooksListAdapter
    private lateinit var books : MutableList<Book>

    lateinit var binding : ActivityBooksListBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBooksListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        books = mutableListOf()
        booksAdapter = BooksListAdapter(books, this)

        with(binding) {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(this@BooksListActivity)
                adapter = booksAdapter

            }
            swipeRefresh.setOnRefreshListener { viewModel.refreshBooks() }
        }


        viewModel.books.observe( this, Observer { newBooks -> updateBooks(newBooks!!) } )
    }

    private fun updateBooks(newBooks: List<Book>) {
        Timber.d("List of books $newBooks")
        books.clear()
        books.addAll(newBooks)
        booksAdapter.notifyDataSetChanged()
        binding.swipeRefresh.isRefreshing = false
    }

    override fun onBookSelected(book: Book) {
        var intent = Intent(this, BookDetailActivity::class.java)
        intent.putExtra(BookDetailActivity.EXTRA_BOOK_ID, book.id)
        startActivity(intent)
    }
}


