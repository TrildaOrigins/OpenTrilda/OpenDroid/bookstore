package com.trilda.bookstore.bookslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.trilda.bookstore.App
import com.trilda.bookstore.Book

class BooksListViewModel: ViewModel() {

    //TODO use injection to get DAO
    val books: LiveData<List<Book>> = App.db.bookDao().getAllBooks()

    fun refreshBooks() {
        App.repository.syncBooksNow()
    }
}