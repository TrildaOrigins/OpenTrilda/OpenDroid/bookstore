package com.trilda.bookstore.repository

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.trilda.bookstore.App
import timber.log.Timber
import training.archcomps.bookstore.repository.FakeBookApi

class SyncRepositoryWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
        Timber.i("Synchronizing books ...")
        val bookApi = FakeBookApi()
        val bookDao = App.db.bookDao()

        bookDao.insertBooks(bookApi.loadBooks())

        return Result.success()
    }
}
