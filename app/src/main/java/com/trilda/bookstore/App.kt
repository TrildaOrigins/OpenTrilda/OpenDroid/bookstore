package com.trilda.bookstore

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import com.trilda.bookstore.database.BookDatabase
import com.trilda.bookstore.database.DATABASE_NAME
import com.trilda.bookstore.repository.BookRepository
import timber.log.Timber

class App : Application() {
    companion object {
        lateinit var db: BookDatabase
        lateinit var repository : BookRepository
    }

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        db = Room.databaseBuilder(this, BookDatabase::class.java, DATABASE_NAME)
                .build()

        Timber.e("Hello Books")
        Timber.i("this is a number %d, and another  texte %s", 42, "toto")

        repository = BookRepository(this)
        repository.scheduleBooksSync()

    }
}